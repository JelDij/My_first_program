import java.util.Arrays;
import java.util.Scanner; // imports scanner

public class Game {
    public char guess; // makes sure user can only enter one character as a guess
    public int score = 10; //initializes the number of guesses or score to 10;
    public String guesses = ""; //this is to ensure the string starts empty
    public boolean match; //this is used to check if there is a match, otherwise score needs to be decreased
    public boolean win; // this is a boolean to check if player has won
    RandomMovie myRandomMovie = new RandomMovie(); //this makes the object myRandomMovie used in this class

    public boolean runRound() { //runs a single round: checks if there is a match and returns boolean if there is at least 1 match
                                //otherwise decreases score and stores your wrong guess
        int matches = 0;

        for (int i = 0; i < myRandomMovie.movieLowerCase.length(); i++) {
            if (guess == myRandomMovie.movieArray[i]) {
                    myRandomMovie.movieUnderscore[i] = guess;
                    matches = +1;
                }
            }
            if (matches > 0) {
                return this.match = true;
            } else {
                String guessString = String.valueOf(guess); //the users guess is a char, so needs to be converted to string
                guesses = guesses + " " + guessString;
                return this.match = false;
            }

        }

        public int keepingScore() { //method to keep score, based on runRound
            if (!match) {
                this.score = this.score - 1;
            }
            return this.score;
        }

        public boolean checkWinCondition() { //checks if arrays are equal, if so turns boolean win to true;
            this.win = Arrays.equals(myRandomMovie.movieUnderscore, myRandomMovie.movieArray);
            return win;
        }

        public void runGame() { //this method runs the whole game!!
            myRandomMovie.randomMovie(); //see objects of RandomMovie class
            myRandomMovie.movieToArray(); //see objects of RandomMovie class
            myRandomMovie.convertMovie(); //see objects of RandomMovie class
            myRandomMovie.deleteSpaces(); //see objects of RandomMovie class
            while (!win) { //only runs if user has not won
                if (score > 0) { // if score is greater than 0, the user has guesses left, so it still runs
                    System.out.println("Your have " + score + " guess(es) left");
                    if (guesses.length() > 0) { //this statements make sure you only see the following from at least one wrong guess onward
                        //otherwise you would see this even if your first guess was right
                        System.out.println("These are the letters you guessed wrong: " + guesses);
                    }
                    System.out.println("You are guessing:");
                    System.out.println(myRandomMovie.movieUnderscore);
                    System.out.println("Guess a letter:");
                    Scanner scanner = new Scanner(System.in);
                    guess = scanner.next().charAt(0);
                    runRound(); //runs a single round
                    keepingScore(); //adjusts user's score if needed
                    checkWinCondition(); //checks if user has won or not
                } else { //this will run if score = 0;
                    System.out.println("Sorry, you lost. The correct movie was: " + myRandomMovie.movie);
                    break;
                }
            }
            if (win) { //this will run if win == true;
                System.out.println(myRandomMovie.movie);
                System.out.println("Congratulations, you guessed the movie with " + score + " guess(es) left!");
            }
        }

        }


