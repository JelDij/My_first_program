import com.sun.xml.internal.bind.v2.runtime.output.StAXExStreamWriterOutput;

import java.io.File; // imports file scanner
import java.io.FileNotFoundException; // imports exception when files not found
import java.util.Scanner; // imports scanner
import java.util.concurrent.ThreadLocalRandom; // imports module to generate random number using range

public class RandomMovie {
    public String movie;
    public String movieLowerCase; //converts movie to lowercase, because Strings are immutable!
    public char[] movieArray;
    public char[] movieUnderscore;

    public String randomMovie() { //this generates a random movie out of a list of 250 movies, which is imported here
        File file = new File("movies.txt"); // imports file
        int randomNum = ThreadLocalRandom.current().nextInt(1, 250 + 1); //generates random number 1-250
        try {
            Scanner fileScanner = new Scanner(file);
            for (int i = 0; i < randomNum; i++) {
                movie = fileScanner.nextLine();//the next line in the file is stored in the string movie until i == randomNum
                movieLowerCase = movie.toLowerCase();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return movieLowerCase; //this is the movie to be guessed
    }

    public char[] movieToArray() {//this converts movieLowerCase to an array, so it can be compared
        movieArray = new char[movieLowerCase.length()]; //you first need to declare the lenght of the array!!!
        for (int i = 0; i < movieLowerCase.length(); i++) {
            movieArray[i] = movieLowerCase.charAt(i);
        }
        return movieArray;
    }

    public char[] convertMovie() { // this converts the movie title to an array storing underscores for every character within the string movie
        movieUnderscore = new char[movieLowerCase.length()];
        for (int i = 0; i < movieLowerCase.length(); i++) {
            movieUnderscore[i] = '_';
        }
        return movieUnderscore;
    }

    public char[] deleteSpaces() {// this deletes the spaces in the movieUnderscore array, because spaces shouldn't be guessed
        for (int i = 0; i < movieLowerCase.length(); i++) {
            if (movieArray[i] == ' ') {
                movieUnderscore[i] = ' ';
            }
        }
        return movieUnderscore;
    }


}






