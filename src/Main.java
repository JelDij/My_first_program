//Created by Jelmer Dijktsra 8-1-2021 - 15-1-2021;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to guess the movie!");
        System.out.println("In this game you have 10 turns to guess the movie displayed.");
        System.out.println("It's basically hangman, but with movies and without a gallows.");
        System.out.println("A few simple instructions:");
        System.out.println("1. You can only guess single letters or numbers. Always use the lower case letter when guessing.");
        System.out.println("2. Some well known movies are sequels and therefore may contain numbers in the title.");
        System.out.println("3. You do not have to consider apostrophes or special symbols like é or : or .");
        System.out.println("4. Your wrong guesses will be displayed. Be careful though, if you guess the wrong letter twice, it will still count as a guess.");
        System.out.println("5. Even if you know the movie title, keep guessing single letters untill the title is complete.");
        System.out.println("6. Have fun!");
        Game newGame = new Game();
        newGame.runGame();
    }
}



